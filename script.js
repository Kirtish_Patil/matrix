const canvas = document.getElementById("canvas1")
canvas.height = window.innerHeight
canvas.width = window.innerWidth

// console.log(canvas.height, canvas.width)

/**@type {CanvasRenderingContext2D} */
const c = canvas.getContext("2d")

//Canvas Setup
c.fillStyle = "black"
c.fillRect(0, 0, canvas.width, canvas.height)

//Code Rain
// const genRandom = () => {
//   let randomNumber = Math.floor(Math.random() * 10)
//   return randomNumber
// }

let matrix =
  "田由甲申甴电甶男甸甹町画甼甽甾甿畀畁畂畃畄畅畆畇畈畉畊畋界畍畎畏畐畑"
// let matrix = ["0", "1"]

matrix = matrix.split("")

const genRandom = () => {
  let randomNumber = Math.floor(Math.random() * 10)
  return randomNumber
}

let font_size = 30
let columns = canvas.width / font_size
let rows = canvas.height / font_size
console.log(columns)
console.log(rows)

let colLen = []

for (let x = 0; x < columns; x++) {
  colLen[x] = 1
}
console.log(colLen)

let column = 1

let y = font_size

// let alpha = 0.7
const draw = () => {
  c.fillStyle = "rgba(0,0,0,0.05)"
  // c.fillStyle = "black"
  c.fillRect(0, 0, canvas.width, canvas.height)

  // c.globalAlpha = alpha
  c.fillStyle = "rgb(0,255,0)"
  c.font = font_size + "px serif"

  for (let x = 0; x < columns; x++) {
    let randomMat = matrix[Math.floor(Math.random() * 10)]
    c.fillText(randomMat, x * font_size, colLen[x] * font_size)
    colLen[x]++
    // alpha = alpha - 0.01

    // if (alpha <= 0) {
    //   alpha = 1
    // }

    if (colLen[x] * font_size > canvas.height && Math.random() > 0.975) {
      colLen[x] = Math.floor(Math.random() * 10)
      colLen[x] = 0
    }
  }
}

// draw()
// draw()
// draw()
// draw()
// draw()
// draw()

setInterval(() => {
  draw()
}, 50)

// c.fillStyle = "green"
// c.font = "50px serif"
// c.fillText(genRandom(), 100, 150)

// let position = 100

// let intervalId = setInterval(() => {
//   c.fillStyle = "rgba(0,0,0,0,5)"
//   c.fillRect(100, (position += 50), 50, 50)
//   c.fillStyle = "green"
//   c.font = "50px serif"
//   c.fillText(genRandom(), 100, (position += 50))
// }, 100)

// let co_ord1 = 50
// let co_ord2 = 100

// const updateNum = (randomNum, size) => {
//   c.fillStyle = "orange"
//   c.fillRect(400, size - 40, 50, 50)

//   c.fillStyle = "green"
//   c.font = "50px serif"

//   c.fillText(`${randomNum}`, 400, size)
// }

// setTimeout(() => {
//   clearInterval(intervalId)
// }, 1000)

// let size = 50
// setInterval(() => {
//   c.fillStyle = "rgba(0,0,0,0.5)"
//   c.fillRect(400, (size += 50 - 40), 50, 50)

//   c.fillStyle = "green"
//   c.font = "50px serif"
//   c.fillText(`${genRandom()}`, 400, size)
// }, 100)
